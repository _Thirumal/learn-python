class Bike:
    def __init__(self, color, frame):
        print("__init__")
        self.color = color
        self.frame = frame
    def brake(self):
        print("Break")

red = Bike("Red", "Fiber")
blue = Bike("Blue", "ABs")

print("Color: ", red.color, " Material: ", red.frame)

print("Color: ", blue.color, " Material: ", blue.frame)