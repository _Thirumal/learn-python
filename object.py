import numpy as np

x = np.array([1, 2, 3])
y = np.array([4, 5, 6, 5, 123])


print("X mean: ", x.mean())

print("Y mean: ", y.mean())

print ("X shape: ", x.shape)

l1 = [1, 2, 3]
l2 = l1

print("l1: ", l1)

print("l2: ", l2)

l1[0] = 123

print("l1: ", l1)

print("l2: ", l2)

n = 7
m= n
print ("n: ", n, " & m: ", m)
n = 4
print ("n: ", n, " & m: ", m)

import sys

print(sys.int_info)